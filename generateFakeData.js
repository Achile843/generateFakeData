import fs from "fs";
import faker from "faker";
import { phrase, sentence } from "coder-ipsum";

let posts = [];
const NUMBER_OF_POSTS = 1000;
// let userID = 1;

class Post {
  constructor() {
    this.post = faker.helpers.createCard();
  }
}
for (let i = 0; i < NUMBER_OF_POSTS; i++) {
  posts.push(new Post());
}

fs.writeFile("./fakeData.json", JSON.stringify(posts, null, 2), (err) => {
  if (err) {
    console.log("There was an error");
  } else {
    console.log("File successfully written.");
  }
});
// qM(:zw.f3c_-v6_
